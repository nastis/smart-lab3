import sofia_kp.KPICore;
import sofia_kp.iKPIC_subscribeHandler;

import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by itp573 on 10.10.2014.
 */


public class SS_Agent implements iKPIC_subscribeHandler {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getLogger(SS_Agent.class.getName());

    public SS_Agent(String HOST, int PORT, String SMART_SPACE_NAME) {
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }

    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String query(String subject, String predicate, String object, String s_type, String o_type) {
        String retXml;
        log.info("doing query");
        retXml = kpi.queryRDF(subject, predicate, object, s_type, o_type);
        return retXml;
    }

    @Override
    public void kpic_SIBEventHandler(String s) {
        synchronized (kpi) {
            Vector<Vector<String>> deleted = kpi.xmlTools.getObsoleteResultEventTriple(s);
            Vector<Vector<String>> inserted = kpi.xmlTools.getNewResultEventTriple(s);
            if ((inserted != null) && (inserted.get(0).get(1).equals("is_on_vacation_from"))) {
                String query = query(inserted.get(0).get(0), "is_back_to_work_on", null, "uri", "literal");
                Vector<Vector<String>> out = kpi.xmlTools.getQueryTriple(query);
                long diff = Long.valueOf(out.get(0).get(2)) - Long.valueOf(inserted.get(0).get(2));

                diff = diff * 2;

                try {
                    insert(inserted.get(0).get(0), "will_get", String.valueOf(diff/1000000), "uri", "literal");
                } catch (SmartSpaceException e) {
                    e.printStackTrace();
                    System.out.println("Something got wrong in insert");

                }

            }
            if ((inserted != null) && (inserted.get(0).get(1).equals("is_ill_from"))) {
                String query = query(inserted.get(0).get(0), "is_awesome_after", null, "uri", "literal");
                Vector<Vector<String>> out = kpi.xmlTools.getQueryTriple(query);
                long diff = Long.valueOf(out.get(0).get(2)) - Long.valueOf(inserted.get(0).get(2));
                diff = diff * 3;

                try {
                    insert(inserted.get(0).get(0), "will_get", String.valueOf(diff/1000000), "uri", "literal");
                } catch (SmartSpaceException e) {
                    e.printStackTrace();
                    System.out.println("Something got wrong in insert");

                }
            }
            //process incoming data
        }
    }

    public void subscribe(String subject, String predicate, String object, String objectType) {
        synchronized (kpi) {
            String retXml = kpi.subscribeRDF(subject, predicate, object, objectType);
            if (retXml != null && kpi.xmlTools.isSubscriptionConfirmed(retXml)) {
                subscriptionIdList.add(kpi.xmlTools.getSubscriptionID(retXml));
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> confirmed ", new Date(), predicate));
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> was aborted ", new Date(), predicate));
                }
            }
        }
    }

    public void unSubscribeAll() {
        String retXml;
        synchronized (kpi) {
            for (String id : subscriptionIdList) {
                retXml = kpi.unsubscribe(id);
                if (kpi.xmlTools.isUnSubscriptionConfirmed(retXml)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription is confirmed.", new Date()));
                    }
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription isn't confirmed", new Date()));
                    }
                }
            }
        }
        subscriptionIdList = new ArrayList<String>();
    }

    public void start() {
        String retXml = kpi.join();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            //log.log(Level.FINE, "Successfully joined to smart space.");
            log.info("Successfully joined to smart space.");
        } else {
            //log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            log.info("Could not join to smart space. Will not function properly!");
            return;
        }
        kpi.setEventHandler(this);
    }

    public void stop() {
        // Cancel all the subscriptions
        unSubscribeAll();

        // Leave smart space
        String retXml = kpi.leave();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    public List<String> getSubscriptionIdList() {
        return subscriptionIdList;
    }

    public void setSubscriptionIdList(List<String> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;

        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public static void main(String[] args) {

        SS_Agent service = new SS_Agent("192.168.100.11", 10010, "X");
        service.start();
        log.info("Started");


        System.out.println("Type 'quit <Enter>' to stop service and leave.\n> ");

        String pred = "is_on_vacation_from";
        String pred1 = "is_ill_from";


        service.subscribe(null, pred, null, "literal");
        service.subscribe(null, pred1, null, "literal");


        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

            while (true) {
                String cmd = bufferedReader.readLine();
                if (cmd.equals("bye")) {
                    break;
                }

            }
        } catch (IOException e) {
            System.out.println("Bye");

        } finally {
            service.stop();
        }





        log.log(Level.INFO, "Bye.");
    }
}

