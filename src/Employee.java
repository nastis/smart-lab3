        import sofia_kp.KPICore;
        import sofia_kp.iKPIC_subscribeHandler;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.util.ArrayList;
        import java.util.Date;
        import java.util.List;
        import java.util.Vector;
        import java.util.logging.Level;
        import java.util.logging.Logger;

/**
 * Created by itp573 on 10.10.2014.
 */


public class Employee implements iKPIC_subscribeHandler {
    private final KPICore kpi;
    private List<String> subscriptionIdList = new ArrayList<String>();
    private static Logger log = Logger.getLogger(Employee.class.getName());

    public Employee(String HOST, int PORT, String SMART_SPACE_NAME) {
        kpi = new KPICore(HOST, PORT, SMART_SPACE_NAME);
        if (log.isLoggable(Level.FINEST))
            kpi.enable_debug_message();
        if (log.isLoggable(Level.FINER))
            kpi.enable_error_message();
    }
    public String insert(String subject, String predicate, String object, String s_type, String objType) throws SmartSpaceException {
        String retXml;
        synchronized (kpi) {
            retXml = kpi.insert(subject,    // subject
                    predicate,  // predicate
                    object,     // object
                    "uri",      // subject type
                    objType);   // object type
            if (retXml != null && kpi.xmlTools.isInsertConfirmed(retXml)) {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("Insert of <%s, %s, %s> succeeded", subject, predicate, object));
                }
            } else {
                log.warning(String.format("Insert of <%s, %s, %s> failed", subject, predicate, object));
                throw new SmartSpaceException("Insert triple failed", retXml);
            }
        }
        return retXml;
    }

    public String query(String subject, String predicate, String object, String s_type, String o_type) throws SmartSpaceException{
        String retXml;
        log.info("doing query");
        retXml = kpi.queryRDF(subject, predicate, object, s_type, o_type);
        return retXml;
    }

    @Override
    public void kpic_SIBEventHandler(String s) {
        synchronized (kpi) {
            Vector<Vector<String>> deleted = kpi.xmlTools.getObsoleteResultEventTriple(s);
            Vector<Vector<String>> inserted = kpi.xmlTools.getNewResultEventTriple(s);
            if ((inserted!=null) && (inserted.get(0).get(1).equals("will_get"))) {
                long refund = Long.valueOf(inserted.get(0).get(2));
                System.out.println("I got refund! Refunded: " + String.valueOf(refund) + "$");

            }
            String id = kpi.xmlTools.getSubscriptionID(s);
            //process incoming data
        }
    }

    public void subscribe(String subject, String predicate, String object, String objectType) {
        synchronized (kpi) {
            String retXml = kpi.subscribeRDF(subject, predicate, object, objectType);
            if (retXml != null && kpi.xmlTools.isSubscriptionConfirmed(retXml)) {
                subscriptionIdList.add(kpi.xmlTools.getSubscriptionID(retXml));
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> confirmed ", new Date(), predicate));
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.fine(String.format("%s : Subscription of <%s> was aborted ", new Date(), predicate));
                }
            }
        }
    }

    public void unSubscribeAll() {
        String retXml;
        synchronized (kpi) {
            for (String id : subscriptionIdList) {
                retXml = kpi.unsubscribe(id);
                if (kpi.xmlTools.isUnSubscriptionConfirmed(retXml)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription is confirmed.", new Date()));
                    }
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.fine(String.format("%s : Unsubscription isn't confirmed", new Date()));
                    }
                }
            }
        }
        subscriptionIdList = new ArrayList<String>();
    }

    public void start() {
        String retXml = kpi.join();
        if (retXml != null && kpi.xmlTools.isJoinConfirmed(retXml)) {
            //log.log(Level.FINE, "Successfully joined to smart space.");
            log.info("Successfully joined to smart space.");
        } else {
            //log.log(Level.SEVERE, "Could not join to smart space. Will not function properly!");
            log.info("Could not join to smart space. Will not function properly!");
            return ;
        }
        kpi.setEventHandler(this);
    }

    public void stop() {
        // Cancel all the subscriptions
        unSubscribeAll();

        // Leave smart space
        String retXml = kpi.leave();
        if (retXml != null && kpi.xmlTools.isLeaveConfirmed(retXml)) {
            log.log(Level.FINE, "Successfully left smart space.");
        } else {
            log.log(Level.WARNING, "Could not leave smart space. It is not fatal but a little but annoying.");
        }
    }

    public List<String> getSubscriptionIdList() {
        return subscriptionIdList;
    }

    public void setSubscriptionIdList(List<String> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    static class SmartSpaceException extends Exception {
        private String ssapResponse;
        SmartSpaceException(String msg, String ssap) {
            super(msg);
            ssapResponse = ssap;
        }

        public String getSsapResponse() {
            return ssapResponse;
        }
    }

    public static void main(String[] args) {

        Employee service = new Employee("192.168.100.11",10010,"X");
        service.start();
        log.info("Started");

        //service.subscribe(null,"http://cais.iias.spb.su/RDF/agent#is_a",null,"literal");

        //BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Type 'quit <Enter>' to stop service and leave.\n> ");

        //String cmd = bufferedReader.readLine();
        //while (true) {
                 /*   if (cmd.equals("exit") || cmd.equals("quit") || cmd.equals("stop")) {
                        break;
                    }
                    if (!cmd.isEmpty()) {
                        System.out.println(String.format("Unknown command: '%s'", cmd));
                    }
                    System.out.print("> ");
                    cmd = bufferedReader.readLine();
                }*/
        String subj = "employee 01";
        String from = "is_on_vacation_from";
        String from1 = "is_ill_from";
        String to = "is_back_to_work_on";
        String to1 = "is_awesome_after";

        long obj = 123456789;
        long obj1 = 245678910;




        try {

            service.subscribe(subj, "will_get", null, "literal");

            service.insert(subj, from, String.valueOf(obj), "uri", "literal");
            service.insert(subj, to, String.valueOf(obj1), "uri", "literal");
            service.insert(subj, from1, String.valueOf(obj), "uri", "literal");
            service.insert(subj, to1, String.valueOf(obj1), "uri", "literal");


            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

                while (true) {
                    String cmd = bufferedReader.readLine();
                    if (cmd.equals("bye")) {
                        break;
                    }

                }
            } catch (IOException e) {
                System.out.println("Bye");

            }
        } catch (SmartSpaceException e) {
            e.printStackTrace();
        } finally {
            service.stop();
        }
        log.log(Level.INFO, "Bye.");
    }
}
